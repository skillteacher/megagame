using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleEnemy : MonoBehaviour
{
    [SerializeField] private float speed = 5f;
    [SerializeField] private string groundLayerName = "Ground";
    private Rigidbody2D enemyRigidbody;
    private bool isFasingRight;

    private void Awake()
    {
        enemyRigidbody = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        Vector2 velocity = 
            new Vector2(isFasingRight ? speed : -speed, enemyRigidbody.velocity.y);

        enemyRigidbody.velocity = velocity;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer != LayerMask.NameToLayer(groundLayerName)) return;

        Flip();
    }
    public void Flip()
    {
        isFasingRight = !isFasingRight;
        transform.Rotate(0f, 180f, 0f);
    }
}
