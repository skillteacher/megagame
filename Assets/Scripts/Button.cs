using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour
{
    [SerializeField] private DeleteWall wall;
    [SerializeField] private string playerLayer = "Player";
    private bool isCollected;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer != LayerMask.NameToLayer(playerLayer)) return;

        PickButton();
    }

    private void PickButton()
    {
        if (isCollected) return;
        wall.DeletedWall();
        isCollected = true;
        Destroy(gameObject);
    }
}
