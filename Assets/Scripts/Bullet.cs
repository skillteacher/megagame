using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private bool isTouched;
    [SerializeField] private Vector2 firstVector;
    private Vector2 vector;
    private float speed = 7f;
    [SerializeField] private Collider2D colliderB;
    [SerializeField] private Rigidbody2D bulletRigidbody;

    private void Awake()
    {
        vector = firstVector;
    }
    private void FixedUpdate()
    {
        Vector2 velocity = new Vector2(vector.x + speed, vector.y);
        bulletRigidbody.velocity = velocity;
        isTouched = colliderB.IsTouchingLayers(LayerMask.GetMask("Ground"));
        isTouched = colliderB.IsTouchingLayers(LayerMask.GetMask("Player"));
        if (isTouched) vector = firstVector;
    }
}