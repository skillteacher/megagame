using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    public static PlayerUI UI;
    [SerializeField] private Text livesText;
    [SerializeField] private Text coinText;

    private void Awake()
    {
        if(UI==null)
        {
            UI = this;
        }
        else
        {
            Destroy(this);
        }
    }
    public void SetLives(int amount)
    {
        livesText.text = amount.ToString();
    }

    public void ShowCoinCount(int amount)
    {
        coinText.text = amount.ToString();
    }
}
