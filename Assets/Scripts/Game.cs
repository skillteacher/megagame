using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
    [SerializeField] private int startLives = 3;
    [SerializeField] private string firstLevel;
    [SerializeField] private GameObject restartFirstLevel;
    [SerializeField] private Text finalCoinCount;
    private int coinCount;
    private int livesCount;
    private void Awake()
    {
        Game[] anotherGame = FindObjectsOfType<Game>();

        if (anotherGame.Length > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        livesCount = startLives;
    }
    public void RestartGame()
    {
        coinCount = 0;
        livesCount = startLives;
        Time.timeScale = 1f;
        restartFirstLevel.SetActive(false);
        SceneManager.LoadScene(firstLevel);
        PlayerUI.UI.SetLives(livesCount);
    }

    private void GameOver()
    {
        Time.timeScale = 0f;
        restartFirstLevel.SetActive(true);
        finalCoinCount.text = coinCount.ToString();
    }

    public void LoseLife()
    {
        livesCount--;
        PlayerUI.UI.SetLives(livesCount);
        if (livesCount <= 0) GameOver();
    }

    public void AddCoins(int amount)
    {
        coinCount += amount;
        PlayerUI.UI.ShowCoinCount(coinCount);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}